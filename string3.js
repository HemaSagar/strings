function string3(date){
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const dates = date.split('/');

    const result = months[parseInt(dates[1],10)-1];
    return result;
}

module.exports = string3;
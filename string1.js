function string1(currency){
    if(typeof(currency) == 'string'){
        let result = "";
        result = currency.replace("$","");
        result = result.replaceAll(",","");
    
        if(!isNaN(result)){
            return parseFloat(result,10);
        }
    }

    return 0;
}

module.exports = string1;
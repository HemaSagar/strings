function string2(ip){
    // if the input is not a string returning an empty array
    if(typeof(ip) != 'string'){
        return [];
    }
    const result = ip.split('.',4);
    for(i=0;i<result.length;i++){
        // if any of component of ip is not a number then returning an empty array
        if(isNaN(result[i])){
            return [];
        }
        result[i] = parseInt(result[i],10); //converting the string components of ip to number format.

        //checking if the components belong to ipv4 or not.
        if(result[i]<0 || result[i]>255){
            return [];
        }
    }
    return result;
}

module.exports = string2;
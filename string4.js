function string4(name){
    const nameKeys = Object.keys(name); 
    let fullName = "";
    //concatinating the name.
    for(i=0;i<nameKeys.length;i++){
        fullName += name[nameKeys[i]] + " ";
    }

    fullName = fullName.trim(); // removing the extra spaces at the corners.
    fullName = fullName.toUpperCase(); //converting to title case;

    return fullName;
}

module.exports = string4;
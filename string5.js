function string5(wordsArray){
    if(typeof(wordsArray) == 'undefined' || wordsArray.length == 0){
        return "";
    }

    //checking if the array contains any element that is not a string.
    for(i=0;i<wordsArray.length;i++){
        if(typeof(wordsArray[i]) != 'string'){
            return "";
        }
    }

    let fullLine = wordsArray.join(" ");
    return fullLine;
}

module.exports = string5;